package com.example.toik11.service.impl;

import com.example.toik11.dto.MovieListDto;
import com.example.toik11.repository.MovieRepository;
import com.example.toik11.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MovieServiceImpl implements MovieService {

    @Autowired
    private MovieRepository movieRepository;

    @Override
    public MovieListDto getMovies() {
        return movieRepository.getMovies();
    }
}
