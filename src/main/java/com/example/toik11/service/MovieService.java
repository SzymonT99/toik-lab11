package com.example.toik11.service;

import com.example.toik11.dto.MovieListDto;

public interface MovieService {

    MovieListDto getMovies();
}
