package com.example.toik11.repository;

import com.example.toik11.dto.MovieListDto;

public interface MovieRepository {

    MovieListDto getMovies();
}
