package com.example.toik11.rest;

import com.example.toik11.dto.MovieListDto;
import com.example.toik11.service.MovieService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MovieApiController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MovieApiController.class);

    private MovieService movieService;

    @Autowired
    public MovieApiController(MovieService movieService) {
        this.movieService = movieService;
    }

    @CrossOrigin
    @GetMapping("/movies")
    public ResponseEntity<MovieListDto> getMovies() {

        LOGGER.info("--- get all movies: {}", movieService.getMovies());

        return new ResponseEntity<>(movieService.getMovies(), HttpStatus.OK);
    }

}

